      subroutine single_ray_trace(vector, density, lstep, x0, y0, 
     &       z0, dx, dy, dz, xmax, ymax, zmax, xmin, ymin, 
     &       zmin, maxstep, nx, ny, nz, nray, colray)
	
	  implicit none
C#######################################################################
      INTEGER*4   i
      INTEGER*4   nx,ny,nz, nray
      INTEGER*4   maxstep,nstep,xindex,yindex,zindex 
      DOUBLE PRECISION  xmin,xmax,ymin,ymax,zmin,zmax,lstep
      DOUBLE PRECISION  x0,y0,z0,dx,dy,dz
      DOUBLE PRECISION  vector(nray,3),density(nx,ny,nz)
 	  
      DOUBLE PRECISION  xcurrent,ycurrent,zcurrent,vx,vy,vz
      DOUBLE PRECISION  colray(nray),xstep,ystep,zstep
      DOUBLE PRECISION  abslength

C      write(*,*) vector
c      write(*,*) density(10,10,10)
c      write(*,*) lstep, x0, y0, z0
c      write(*,*) lstep, nx, ny, nz


c      write(*,*) vector(5,1), vector(5,2), vector(5,3)

      DO i=1, nray      

      xcurrent = x0
      ycurrent = y0
      zcurrent = z0


      vx = vector(i,1) 
      vy = vector(i,2)
      vz = vector(i,3)

      nstep = 0
      colray(i) = 0.0d0

      xstep = dx * vx * lstep
      ystep = dy * vy * lstep
      zstep = dz * vz * lstep

      abslength = sqrt(xstep**2. + ystep**2. + zstep**2.)

      xcurrent = (nstep * xstep) + x0
      ycurrent = (nstep * ystep) + y0
      zcurrent = (nstep * zstep) + z0

      DO WHILE ( (xcurrent .LT. xmax) .AND. (xcurrent .GE. xmin) .AND. 
     &       (ycurrent .LT. ymax) .AND. (ycurrent .GE. ymin) .AND. 
     &       (zcurrent .LT. zmax) .AND. (zcurrent .GE. zmin) )

      xindex = INT((xcurrent - xmin) / dx) + 1
      yindex = INT((ycurrent - ymin) / dy) + 1
      zindex = INT((zcurrent - zmin) / dz) + 1

c      if (i .eq. 5) then
c        write(*,*) nstep, xcurrent, ycurrent, zcurrent
c        write(*,*) nstep, xindex, yindex, zindex
c        write(*,*) nstep, xindex, yindex, zindex, 
c     &           density(xindex,yindex,zindex)
c         write(*,*) nstep, vx, vy, vz
c      endif
      
      IF ( (ABS(xcurrent-x0) .GT. (dx * 0.5) ) .OR. ( ABS(ycurrent-y0) 
     &   .GT. (dy * 0.5) ) .OR. ( ABS(zcurrent-z0) 
     &   .GT. (dz * 0.5) ) ) THEN
         colray(i)=colray(i)+(density(xindex,yindex,zindex) * abslength)
      ENDIF 

      nstep = nstep + 1

      xcurrent = DBLE(nstep * xstep) + x0
      ycurrent = DBLE(nstep * ystep) + y0
      zcurrent = DBLE(nstep * zstep) + z0

      IF ( nstep .GT. maxstep ) THEN
        WRITE(*,*) 'WARNING MAXSTEP = ',maxstep,' is reached in ', 
     &            x0, y0, z0,' STOPPING!'
        STOP
      ENDIF

      END DO
 
      END DO


      RETURN 
      END subroutine single_ray_trace	

      subroutine all_ray_trace(vector, density, lstep, x, y, 
     &       z, dx, dy, dz, xmax, ymax, zmax, xmin, ymin, 
     &       zmin, maxstep, nx, ny, nz, nray, avg_av, lmin)
	
	  implicit none
C#######################################################################
      INTEGER*4   i, j, k, l
      INTEGER*4   nx,ny,nz, nray
      INTEGER*4   maxstep,nstep,xindex,yindex,zindex 
      DOUBLE PRECISION  xmin,xmax,ymin,ymax,zmin,zmax,lstep
      DOUBLE PRECISION  x(nx),y(ny),z(nz),dx,dy,dz
      DOUBLE PRECISION  vector(nray,3),density(nx,ny,nz)
      DOUBLE PRECISION  avg_av(nx,ny,nz)
	  
      DOUBLE PRECISION  xcurrent,ycurrent,zcurrent,vx,vy,vz
      DOUBLE PRECISION  colray(nray),xstep,ystep,zstep
      DOUBLE PRECISION  abslength,local_Av, av(nray)

c Extra output variables: steps taken until the grid edge (n), mean density 
c along the ray (avgdens)

      DOUBLE PRECISION  lpix(nray), lmin(nx,ny,nz)

c      write(*,*) vector
c      write(*,*) density(10,10,10)
c      write(*,*) lstep, x0, y0, z0
c      write(*,*) lstep, nx, ny, nz

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,l,xcurrent,ycurrent,
!$OMP& zcurrent,vx,vy,vz,local_av,colray,nstep,lpix,
!$OMP& xstep,ystep,zstep,abslength,xindex,yindex,zindex)
!$OMP DO
      
      DO i=1, nx  
       DO j=1, ny
        DO k=1, nz

      local_Av = 0.25*dx*density(i,j,k)*5.34788D-22


      DO l=1, nray      

         xcurrent = x(i)
         ycurrent = y(j)
         zcurrent = z(k)


         vx = vector(l,1) 
         vy = vector(l,2)
         vz = vector(l,3)

         nstep = 0
         colray(l) = 0.0d0

         xstep = dx * vx * lstep
         ystep = dy * vy * lstep
         zstep = dz * vz * lstep

         abslength = sqrt(xstep**2. + ystep**2. + zstep**2.)

         xcurrent = DBLE(nstep * xstep) + x(i)
         ycurrent = DBLE(nstep * ystep) + y(j)
         zcurrent = DBLE(nstep * zstep) + z(k)

      DO WHILE ( (xcurrent .LT. xmax) .AND. (xcurrent .GE. xmin) .AND. 
     &       (ycurrent .LT. ymax) .AND. (ycurrent .GE. ymin) .AND. 
     &       (zcurrent .LT. zmax) .AND. (zcurrent .GE. zmin) )

      xindex = INT((xcurrent - xmin) / dx) + 1
      yindex = INT((ycurrent - ymin) / dy) + 1
      zindex = INT((zcurrent - zmin) / dz) + 1

      IF ((xindex .LT. 1) .OR. (xindex .GT. nx) .OR. (yindex .LT. 1) 
     &  .OR. (yindex .GT. ny) .OR. (zindex .LT. 1) .OR. 
     &  (zindex .GT. nz) ) THEN
         EXIT
      ENDIF

!      IF ( (ABS(xcurrent-x(i)) .GT. (dx * 0.5)) .OR. (ABS(ycurrent-y(j)) 
!     &   .GT. (dy * 0.5) ) .OR. ( ABS(zcurrent-z(k)) 
!     &   .GT. (dz * 0.5) ) ) THEN
         colray(l)=colray(l)+(density(xindex,yindex,zindex) * abslength)
!      ENDIF                                                         
      
      nstep = nstep + 1

      xcurrent = DBLE(nstep * xstep) + x(i)
      ycurrent = DBLE(nstep * ystep) + y(j)
      zcurrent = DBLE(nstep * zstep) + z(k)

      IF ( nstep .GT. maxstep ) THEN
        WRITE(*,*) 'WARNING MAXSTEP = ',maxstep,' is reached in ', 
     &            x(i), y(j), z(k),' STOPPING!'
        STOP
      ENDIF

      END DO

      lpix(l) = DBLE(nstep) * abslength
      av(l) = exp(-2.5  * colray(l) * 5.34788D-22)   ! convert to Av the do the weighting

      END DO
   
c lets calculate the effective Av -- weighted for the CO shielding, according
c    <Av> = -1/2.5 * ln(1/npix * sum( exp(-2.5 * Av(1..npix)) ))
   
   
!      avg_av(i,j,k)=sum( (colray * 5.34788D-22) ) / DBLE(nray) !+local_Av !not needed
!      avg_av(i,j,k) = minval(colray * 5.34788D-22)  ! use this for better comparison
                                                    ! with the earlier figures (six-ray)
      ! on a more detailed comparison: this is a *very* bad way of computing an 
      ! effective Av: since we have a lot of rays, the minimum (or the maximux)
      ! Av represents only a small surface fraction of the whole sky, the <Av> is better!!

      avg_av(i,j,k) = -0.4d0 * log( sum(av) / DBLE(nray) )

      lmin(i,j,k) = minval(lpix)

c      write(*,*) i, j, k, avg_av(i,j,k), sum(colray * 5.34788D-22)

      END DO
      END DO
      END DO
!$OMP END DO
!$OMP END PARALLEL

      RETURN 
      END subroutine all_ray_trace	
